<?php

namespace SelectDb\contract;

interface Arrayable
{
    public function toArray(): array;
}
