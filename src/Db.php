<?php

namespace SelectDb;

use SelectDb\db\DbManager;


/**
 * @see \SelectDb\db\DbManager
 * @mixin \SelectDb\db\DbManager
 * @method static \SelectDb\db\Query procedure(bool $procedure = true)
 * @method static \SelectDb\db\BaseQuery table($table)
 * @method static \SelectDb\db\BaseQuery field($field)
 * @method static \SelectDb\db\BaseQuery query(string $sql, array $bind = [])
 * @method static \SelectDb\db\BaseQuery execute(string $sql, array $bind = [])
 * @method static \SelectDb\db\concern\Transaction startTrans()
 * @method static \SelectDb\db\concern\Transaction commit()
 * @method static \SelectDb\db\concern\Transaction rollback()
 * @method static \SelectDb\db\concern\JoinAndViewQuery view($join, $field = true, $on = null, string $type = 'INNER', array $bind = [])
 */
class Db extends Facade
{
    protected static $pk = 'id';
    protected static $table = '';
    protected static $prefix = 'pre_';
    protected static $schema = [];
    
    public static function db($getSql = false)
    {
        $config = [
            'pk'     => static::$pk,
            'table'  => static::$table,
            'prefix' => static::$prefix,
            'schema' => static::$schema,
            'getSql' => $getSql,
        ];
        return new DbManager($config);
    }
    
    public static function __callStatic($method, $params)
    {
        return static::db()->$method(...$params);
    }
    
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     *
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'SelectDb\db\DbManager';
    }
}

