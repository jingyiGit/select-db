<?php
declare (strict_types=1);

namespace SelectDb\db\concern;

/**
 * 数据字段信息
 */
trait TableFieldInfo
{
    
    /**
     * 获取数据表字段信息
     *
     * @access public
     * @param string $tableName 数据表名
     * @return array
     */
    public function getTableFields($tableName = ''): array
    {
        if ('' == $tableName) {
            $tableName = $this->getTable();
        }
        return $this->discuz_getFields($tableName);
    }
    
    /**
     * 获取详细字段类型信息
     *
     * @access public
     * @param string $tableName 数据表名称
     * @return array
     */
    public function getFields(string $tableName = ''): array
    {
        return $this->discuz_getFields($tableName ?: $this->getTable('', true));
    }
    
    /**
     * 获取字段类型信息
     *
     * @access public
     * @return array
     */
    public function getFieldsType(): array
    {
        if (!empty($this->options['field_type'])) {
            return $this->options['field_type'];
        }
        return $this->discuz_getFieldType($this->getTable('', true));
    }
    
    /**
     * 获取字段类型信息
     *
     * @access public
     * @param string $field 字段名
     * @return string|null
     */
    public function getFieldType(string $field)
    {
        $fieldType = $this->getFieldsType();
        
        return $fieldType[$field] ?? null;
    }
    
    /**
     * 获取字段类型信息
     *
     * @access public
     * @return array
     */
    public function getFieldsBindType(): array
    {
        if (!empty($this->options['field_type'])) {
            $bind = [];
            foreach ($this->options['field_type'] as $field_name => $field_type) {
                $bind[$field_name] = $this->getFieldBindType1($field_type);
            }
            return $bind;
        }
        
        return $this->discuz_getFieldType($this->getTable('', true), 'int');
    }
    
    /**
     * 获取字段绑定类型
     *
     * @access public
     * @param string $type 字段类型
     * @return integer
     */
    public function getFieldBindType1(string $type): int
    {
        $bindType = [
            'string'    => 2,  // PDO::PARAM_STR
            'str'       => 2,  // PDO::PARAM_STR
            'integer'   => 1,  // PDO::PARAM_INT
            'int'       => 1,  // PDO::PARAM_INT
            'boolean'   => 5,  // PDO::PARAM_BOOL
            'bool'      => 5,  // PDO::PARAM_BOOL
            'float'     => 21, // self::PARAM_FLOAT
            'datetime'  => 2,  // PDO::PARAM_STR
            'timestamp' => 2,  // PDO::PARAM_STR
        ];
        if (in_array($type, ['integer', 'string', 'float', 'boolean', 'bool', 'int', 'str'])) {
            $bind = $bindType[$type];
        } elseif (0 === strpos($type, 'set') || 0 === strpos($type, 'enum')) {
            $bind = 2; // PDO::PARAM_STR
        } elseif (preg_match('/(double|float|decimal|real|numeric)/is', $type)) {
            $bind = 21; // self::PARAM_FLOAT
        } elseif (preg_match('/(int|serial|bit)/is', $type)) {
            $bind = 1; // PDO::PARAM_INT
        } elseif (preg_match('/bool/is', $type)) {
            $bind = 5; // PDO::PARAM_BOOL
        } else {
            $bind = 2; // PDO::PARAM_STR
        }
        return $bind;
    }
    
    /**
     * 获取字段类型信息
     *
     * @access public
     * @param string $field 字段名
     * @return int
     */
    public function getFieldBindType(string $field): int
    {
        $fieldsType = $this->getFieldsBindType();
        return $fieldsType[$field];
    }
}
